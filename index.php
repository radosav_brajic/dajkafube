<html>
    <head>
        <!-- Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

	<title>Daj Kafu Be!</title>
	<link rel='shortcut icon' type='image/x-icon' href='/img/favicon.ico' />
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
	<div class="bg"></div>
        <section id="loginform" class="outer-wrapper">
            <div class="inner-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-7 form-container">
                            <h2 class="text-center coffee-text">Oces Kafu?</h2>
                            <div class="alert alert-coffee" style="display: none" role="alert">
                                Trazio sam Maji! ( Pogledaj slack )
                            </div>
                            <div class="alert alert-danger" style="display: none" role="alert">
                                Nema kafa za tebe!
                            </div>
                            <form role="form" id="coffee">
                                <div class="form-group">
                                    <input required type="text" class="form-control" id="name" name="name" placeholder="Unesi Ime">
                                </div>
                                <button type="submit" class="btn btn-default coffee-color" style="width:100%">Ocu Kafu!</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <script type="text/javascript">
        // Variable to hold request
        var request;

        // Bind to the submit event of our form
        $("#coffee").submit(function(event){

            // Prevent default posting of form - put here to work in case of errors
            event.preventDefault();

            // Abort any pending request
            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Serialize the data in the form
            var serializedData = $(this).serialize();

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/send.php",
                type: "post",
                data: serializedData
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                // Log a message to the console
                if(response){
                    $('.alert-success').show();
                }else{
                    $('.alert-danger').show();
                }
                setTimeout(function () {
                    $('.alert').fadeOut();
                },2500);
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.error(
                    "The following error occurred: "+
                    textStatus, errorThrown
                );
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });
        });
    </script>
</html>



